package com.topicus.agenda.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.topicus.agenda.model.Afspraak;

public interface AfspraakRepository extends JpaRepository<Afspraak, Long> {
    
}
