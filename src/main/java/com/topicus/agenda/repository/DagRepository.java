package com.topicus.agenda.repository;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.JpaRepository;

import com.topicus.agenda.model.Dag;

public interface DagRepository extends JpaRepository<Dag, Long> {
    Dag findByDatum(LocalDate datum);
}
