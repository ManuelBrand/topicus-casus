package com.topicus.agenda.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.topicus.agenda.exception.UserNotFoundException;
import com.topicus.agenda.model.Agenda;
import com.topicus.agenda.repository.AgendaRepository;
import com.topicus.agenda.request.AfspraakRequest;
import com.topicus.agenda.request.TijdslotRequest;
import com.topicus.agenda.service.AgendaService;

@RestController
@RequestMapping("/agenda/{userId}/")
public class AgendaRestController {
	
	private final AgendaRepository agendaRepository;
	private final AgendaService agendaService;
	
	@Autowired
	AgendaRestController(AgendaRepository agendaRepository, AgendaService agendaService) {
		this.agendaRepository = agendaRepository;
		this.agendaService = agendaService;
	}

	@RequestMapping(method = RequestMethod.GET)
	Agenda readAgenda(@PathVariable String userId) {
		this.validateUser(userId);
		return agendaService.getAgenda(userId);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/tijdslot/")
	void geefEersteVrijeSlotVanaf(@PathVariable String userId, TijdslotRequest req) {
		this.validateUser(userId);
		System.out.println(agendaService.getTijdslot(req));
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity addAfspraak(@PathVariable String userId, AfspraakRequest req) {
		this.validateUser(userId);
		return agendaService.createAfspraak(userId, req);
	}
	
	private void validateUser(String userId) {
		if (this.agendaRepository.findByUsername(userId) == null) {
			throw new UserNotFoundException(userId);
		}
	}
}
