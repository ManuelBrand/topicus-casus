package com.topicus.agenda.helper;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public abstract class DateHelper {

	public static LocalDate stringToLocalDate(String datum) {
		return LocalDate.parse(datum,
	            			   DateTimeFormatter.ofPattern("dd-LL-yyyy"));
	}
	
	public static String localDateToString(LocalDate date) {
		DateTimeFormatter dateFormatter = DateTimeFormatter
	            						  .ofPattern("dd-LL-yyyy");
		return date.format(dateFormatter);
	}
}
