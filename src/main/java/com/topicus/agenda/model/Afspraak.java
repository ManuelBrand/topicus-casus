package com.topicus.agenda.model;

import java.time.LocalTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Afspraak {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
	
	@ManyToOne
    private Dag dag;
	
	private LocalTime start;
	private LocalTime eind;
	private String description;
	
	@SuppressWarnings("unused")
	private Afspraak() { } // JPA only
	
	public Afspraak(Dag dag, LocalTime start, LocalTime eind, String description) {
		this.dag = dag;
		this.start = start;
		this.eind = eind;
		this.description = description;
	}
	
	public LocalTime getStart() {
		return start;
	}
	
	public LocalTime getEind() {
		return eind;
	}
	
	public String getDescription() {
		return description;
	}
}
