package com.topicus.agenda.request;

public class TijdslotRequest {

	private String tijdslot;
	private String datum;
	
	public TijdslotRequest(String tijdslot, String datum) {
		this.tijdslot = tijdslot; 
		this.datum = datum;
	}
	
	public String getTijdslot() {
		return tijdslot;
	}
	
	public String getDatum() {
		return datum;
	}
}
