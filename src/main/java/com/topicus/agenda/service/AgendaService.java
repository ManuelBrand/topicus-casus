package com.topicus.agenda.service;

import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.topicus.agenda.helper.DateHelper;
import com.topicus.agenda.model.Afspraak;
import com.topicus.agenda.model.Agenda;
import com.topicus.agenda.model.Dag;
import com.topicus.agenda.repository.AfspraakRepository;
import com.topicus.agenda.repository.AgendaRepository;
import com.topicus.agenda.repository.DagRepository;
import com.topicus.agenda.request.AfspraakRequest;
import com.topicus.agenda.request.TijdslotRequest;

@Service
public class AgendaService {

	private final AfspraakRepository afspraakRepository;
	private final DagRepository dagRepository;
	private final AgendaRepository agendaRepository;
	
	public Agenda getAgenda(String userId) {
		return sortAgenda(agendaRepository.findByUsername(userId));
	}
	
	public AgendaService(AfspraakRepository afspraakRepository,
						   DagRepository dagRepository,
						   AgendaRepository agendaRepository) {
		this.afspraakRepository = afspraakRepository;
		this.dagRepository = dagRepository;
		this.agendaRepository = agendaRepository;
	}
	
	@SuppressWarnings("rawtypes")
	public ResponseEntity createAfspraak(String userId, AfspraakRequest req) {
		// check !start <= eind = 400 
		// check date niet in verleden = 400 
		if (controleerOfAfspraakOverlapt(req)) {
			return new ResponseEntity(HttpStatus.CONFLICT);
		}
		
		Dag dag = dagRepository.findByDatum(DateHelper.stringToLocalDate(req.getDatum()));
		
		if (dag != null) {
			afspraakRepository.save(new Afspraak(dag,
											 	 LocalTime.parse(req.getStart()),
											 	 LocalTime.parse(req.getEind()),
											 	 req.getDescription()));
			return new ResponseEntity(HttpStatus.CREATED);
		} else {
			Agenda agenda = agendaRepository.findByUsername(userId);
			Dag newDag = dagRepository.save(new Dag(DateHelper.stringToLocalDate(req.getDatum()),
												    agenda));
			afspraakRepository.save(new Afspraak(newDag,
		 	 									 LocalTime.parse(req.getStart()),
		 	 									 LocalTime.parse(req.getEind()),
		 	 									 req.getDescription()));
			return new ResponseEntity(HttpStatus.CREATED);
		}
	}
	
	public LocalTime getTijdslot(TijdslotRequest req) {
		LocalTime startTijd = LocalTime.parse(req.getTijdslot());
		Dag dag = dagRepository.findByDatum(DateHelper.stringToLocalDate(req.getDatum()));

		if (dag == null) {
			// dag bestaat nog niet, dus ook geen afspraken op die dag
			return LocalTime.parse(req.getTijdslot());
		} else {
			sortAfspraken(dag.getAfspraken());
			startTijd = vindOpenTijdslot(dag, startTijd);
		}
		
		return startTijd;
	}
	
	public LocalTime vindOpenTijdslot(Dag dag, LocalTime time) {
		LocalTime startTijd = time;
		Optional<Afspraak> result =
				dag.getAfspraken().stream().filter(obj -> 
					valtStartTijdTussenTweeTijden(startTijd, obj.getStart(), obj.getEind())).findFirst();
		if (result.isPresent()) {
			return vindOpenTijdslot(dag, result.get().getEind());
//			time = result.get().getEind();
		} 
		return time;
	}
	
	private boolean controleerOfAfspraakOverlapt(AfspraakRequest req) {
		Dag dag = dagRepository.findByDatum(DateHelper.stringToLocalDate(req.getDatum()));

		if (dag == null) {
			// dag bestaat nog niet in agenda, dus ook geen afspraken op die dag
			return false;
		} else {
			sortAfspraken(dag.getAfspraken());
			// start van eerste afspraak op die dag is na zowel de start- en eindtijd van de request-afspraak
			Afspraak eersteAfspraak = dag.getAfspraken().get(0);
			if (eersteAfspraak.getStart().isAfter(LocalTime.parse(req.getStart()))
					&& eersteAfspraak.getStart().isAfter(LocalTime.parse(req.getEind()))) {
				return false;
			} else {
				// controleren of de starttijd van request tussen het begin- en eindtijd van een afspraak zit, 
				// en of de eindtijd van request niet tussen het begin en eind van een afspraak ligt
				return dag.getAfspraken().stream().anyMatch(obj -> 
					valtStartTijdTussenTweeTijden(LocalTime.parse(req.getStart()), obj.getStart(), obj.getEind())
					|| valtEindTijdTussenTweeTijden(LocalTime.parse(req.getEind()), obj.getStart(), obj.getEind()));
				// TODO: check of een afspraak niet een totale afspraak op eet.
			}		
		}
	}
	
	private boolean valtStartTijdTussenTweeTijden(LocalTime target, LocalTime start, LocalTime eind) {
		return ((!target.isBefore(start) && target.isBefore(eind)));
	}
	
	private boolean valtEindTijdTussenTweeTijden(LocalTime target, LocalTime start, LocalTime eind) {
		return ((target.isAfter(start) && !target.isAfter(eind)));
	}
	
	private Agenda sortAgenda(Agenda agenda) {
		sortDagen(agenda.getDagen());
		agenda.getDagen().forEach( 
						 d -> sortAfspraken(d.getAfspraken()));
		return agenda;
	}
	
	private void sortDagen(List<Dag> dagen) {
		dagen.sort(
				 (o1, o2) -> 
				  o1.getDatum().compareTo(o2.getDatum()));
	}
	
	private void sortAfspraken(List<Afspraak> afspraken) {
		afspraken.sort(
				 (o1, o2) -> 
				 o1.getStart().compareTo(o2.getStart()));
	}
	
	
	
}
