package com.topicus.agenda.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.topicus.agenda.model.Agenda;

public interface AgendaRepository extends JpaRepository<Agenda, Long> {
    Agenda findByUsername(String username);
}
