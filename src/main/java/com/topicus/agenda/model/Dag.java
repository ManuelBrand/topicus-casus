package com.topicus.agenda.model;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Dag {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
	private LocalDate datum;
	private DayOfWeek dagNaam;
	
	@ManyToOne
    private Agenda agenda;
	
	@OneToMany(mappedBy = "dag")
	private List<Afspraak> afspraken = new ArrayList<>();
	
	@SuppressWarnings("unused")
	private Dag() { } // JPA only

	public Dag (LocalDate datum, Agenda agenda) {
		this.datum = datum;
		this.agenda = agenda;
		this.dagNaam = datum.getDayOfWeek();
	}
	
	public Long getId() {
		return id;
	}
	
	public LocalDate getDatum() {
		return datum;
	}
	
	public DayOfWeek getDagNaam() {
		return dagNaam;
	}
	
	public List<Afspraak> getAfspraken() {
		return afspraken;
	}
}
