package com.topicus.agenda.request;

public class AfspraakRequest {
	
	private String datum;
	private String start;
	private String eind;
	private String description;
	
	public AfspraakRequest(String datum, String start, String eind, String description) {
		this.datum = datum;
		this.start = start;
		this.eind = eind;
		this.description = description;
	}
	
	public String getDatum() {
		return datum;
	}
	
	public String getStart() {
		return start;
	}
	
	public String getEind() {
		return eind;
	}
	
	public String getDescription() {
		return description;
	}
}

