package com.topicus.agenda;

import java.time.LocalTime;
import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.topicus.agenda.helper.DateHelper;
import com.topicus.agenda.model.Afspraak;
import com.topicus.agenda.model.Agenda;
import com.topicus.agenda.model.Dag;
import com.topicus.agenda.repository.AfspraakRepository;
import com.topicus.agenda.repository.AgendaRepository;
import com.topicus.agenda.repository.DagRepository;

@SpringBootApplication
public class AgendaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgendaApplication.class, args);
	}
	
	@Bean
	CommandLineRunner init(AgendaRepository agendaRepository, 
						   DagRepository dagRepository,
						   AfspraakRepository afspraakrepository) {
		return (evt) -> 
				Arrays.asList("manuel".split(","))
				.forEach(
						a -> {
							Agenda agenda = agendaRepository.save(new Agenda(a));
							Dag dag0 = dagRepository.save(new Dag(DateHelper.stringToLocalDate("26-04-2018"), agenda));
							afspraakrepository.save(new Afspraak(dag0, LocalTime.parse("13:00"), 
					                LocalTime.parse("14:00"), "Sollicitatie Manuel."));
							
							Dag dag1 = dagRepository.save(new Dag(DateHelper.stringToLocalDate("24-04-2018"), agenda));
							afspraakrepository.save(new Afspraak(dag1, LocalTime.parse("11:00"), 
					                LocalTime.parse("12:00"), "Retrospective."));
							afspraakrepository.save(new Afspraak(dag1, LocalTime.parse("12:00"), 
					                LocalTime.parse("14:30"), "Werkoverleg Appteam."));
							afspraakrepository.save(new Afspraak(dag1, LocalTime.parse("14:30"), 
					                LocalTime.parse("15:00"), "Pokersessie & Sprint planning."));
							afspraakrepository.save(new Afspraak(dag1, LocalTime.parse("15:00"), 
					                LocalTime.parse("16:30"), "Gifkikkers drinken."));
							
							Dag dag2 = dagRepository.save(new Dag(DateHelper.stringToLocalDate("25-04-2018"), agenda));
							afspraakrepository.save(new Afspraak(dag2, LocalTime.parse("11:00"), 
					                LocalTime.parse("14:00"), "Presentaties Teams."));
						});
	}
}
