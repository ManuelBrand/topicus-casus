package com.topicus.agenda.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Agenda {
	
	@Id
    @GeneratedValue
    private Long id;
	private String username;
	
    @OneToMany(mappedBy = "agenda")
    private List<Dag> dagen = new ArrayList<>();

    @SuppressWarnings("unused")
	private Agenda() { } // JPA only

    public Agenda(final String username) {
        this.username = username;
    }

    public Long getId() {
        return id;
    }
    
    public String getUsername() {
    	return username;
    }

    public List<Dag> getDagen() {
        return dagen;
    }
}
